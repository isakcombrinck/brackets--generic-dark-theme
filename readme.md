# Generic Dark (Brackets Theme)

A subtly colored dark theme focused on readability.
## Screenshots
<img src="screenshots/html.PNG" alt="html">
<img src="screenshots/javascript.PNG" alt="javascript">